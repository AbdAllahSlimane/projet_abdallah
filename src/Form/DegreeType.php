<?php

namespace App\Form;

use App\Entity\Candidate;
use App\Entity\Degree;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

class DegreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    { 
        $builder
            ->add('name',TextType::class, [
                'label' => 'name'
            ])
            ->add('acronym',TextType::class, [
                    'constraints' => [
                        new Constraints\Length([
                            'min' => 3,
                            'max' => 10,
                            'exactMessage' => 'The acronym must contain {{ limit }} character'
                        ])
                        
                    ],
                'label' => 'Acronym'

            ])
            ->add('degree_level',TextType::class, [
                'constraints' => [
                    new Constraints\Length([
                        'min' => 1,
                        'max' => 8,
                        'exactMessage' => 'The Degree level must contain {{ limit }} character (I,II,III,IV,V, VI, VII, VIII)'
                    ])
                    
                ],
                'label' => 'Degree level'
            ]) 
            ->add('candidate')
  
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Degree::class,
        ]);
    } 
}
