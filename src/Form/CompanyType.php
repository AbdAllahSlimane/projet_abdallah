<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, [
                'label' => 'Name of the company'
            ])
            ->add('domain',TextType::class, [
                'label' => 'Domain'
            ])
            ->add('nationality',TextType::class, [
                'label' => 'Nationality' 
            ])
            ->add('creation_date',DateType::class, [
                'widget' => 'single_text', 
                'required' => false
            ])
            ->add('street', TextType::class,[
                'label' => 'Street'
            ])
            ->add('zip_code',NumberType::class,[
                'constraints' => [
                    new Constraints\Length([
                    'min' => 5,
                    'max' => 5,
                    'exactMessage' => 'The zip code must composed of 5 figure'
                ]),
                    new Constraints\Regex([
                    "pattern" => '/[0-9]{5}/',
                    "message" => 'The zip code must composed of 5 figure from 0 to 9'
                ])
            ],
            ])
            ->add('city',TextType::class, [
                'label' => 'City'
            ])
            ->add('country',TextType::class, [
                'label' => 'Country'
            ])
            ->add('user',EntityType::class, [
                'class' => User::class,
                'choice_label' => 'username',
                'multiple' => true,
                'expanded' => true,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
