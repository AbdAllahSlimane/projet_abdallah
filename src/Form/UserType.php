<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username',TextType::class, [
                'label' => 'Username'
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                       'membre' => 'ROLE_USER',
                       'admin' => 'ROLE_ADMIN'
                ],
                'required' => true,
                'multiple' => true,
                "label" => "Roles"
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'constraints' => [
                    new Constraints\Length([
                        'min' => 6,
                        'max' => 20,
                        'minMessage' => "The password must contain at least {{ limit }} character",
                    ]),
                    new Constraints\Regex([
                        "pattern" => '/^(?=.*[A-z])(?=.*[0-9])(?=.*[$@])(\S{6,20})$/',
                        "message" => 'The Username of the mail must contain /^[A-Za-z0-9]+(?:[_]{0,2}[A-Za-z0-9]+)*$/'
                    ]),
                    new Constraints\NotBlank([
                        'message' => 'Please enter the password :',
                    ])
                ]
            ])
            ->add('lastname',TextType::class, [
                'label' => 'Lastname'
            ])
            ->add('firstname',TextType::class, [
                'label' => 'FirstName'
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Mr' => 'Man',
                    'Mme' => 'Woman',
                    'Other' => 'Other',
                ],
            ])
            ->add('age',NumberType::class, [
                'constraints' => [
                    new Constraints\Length ([
                        'min' => 2,
                        'max' => 2,
                        'exactMessage' => "If empty, the age must contain {{ limit }} character",
                    ])
                    
                ],
                'label' => 'Age'
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new Constraints\Regex([
                        "pattern" => "/[0-9]{10}/",
                        'message' => "The phone number is not valid"
                    ])
                ],
            'label' => 'Phone *',

            ])
            ->add('mail', TextType::class, [
                'constraints' => [
                    new Constraints\Email([
                        'message' => 'The email "{{ value }}" is not a valid email.',
                        'checkMX' => true,
                    ]),
                    new Constraints\Length([
                        'min' => 5,
                        'max' => 50,
                        'exactMessage' => 'The mail must composed between 5 to 30 character'
                    ]),
                        new Constraints\Regex([
                        "pattern" => '/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/',
                        "message" => 'The Username of the mail must contain /^[A-Za-z0-9]+(?:[_]{0,2}[A-Za-z0-9]+)*$/'
                    ])
                        
                ],
                'label' => 'Email'
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
