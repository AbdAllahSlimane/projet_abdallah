<?php

namespace App\Controller;

use App\Entity\Candidate;
use App\Form\FormRechercheType;
use App\Repository\CandidateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FormRechercheTypeController extends AbstractController
{
 /**
     * @Route("/", name="home")
     */
    public function search(Request $request, CandidateRepository $candidateRepository, EntityManagerInterface $manager)
    {
       $nouvelleRecherche = new Candidate;
       $formRecherche = $this->createForm(FormRechercheType::class, $nouvelleRecherche);
       $formRecherche->handleRequest($request);

       if($formRecherche->isSubmitted()){
           if ($formRecherche->isValid()){
               $manager->persist($nouvelleRecherche);
               $manager->flush();

               $candidateSearch = $request->query->get("candidate_id");
               if($candidateSearch){
                   $formRecherche = $candidateRepository->findBySearch($candidateSearch);
               }else{
                $this->addFlash('warning', 'Soit le formulaire n\'est pas valide, soit le candidat ne possède pas ce diplôme :)');
               } 
            }
       }
       $formRecherche = $formRecherche->createView();
        return $this->render('home/index.html.twig', [
            'formRecherche' => $formRecherche,
        ]);
    }

}
