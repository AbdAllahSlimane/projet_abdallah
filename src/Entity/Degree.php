<?php

namespace App\Entity;

use App\Entity\Candidate;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DegreeRepository")
 */
class Degree
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $acronym;
 
    /**
     * @ORM\Column(type="string", length=5)
     */
    private $degree_level;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Candidate", mappedBy="degree", cascade={"persist", "remove"})
     * 
     */
    private $candidate;

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronym(): ?string
    {
        return $this->acronym;
    }

    public function setAcronym(string $acronym): self
    {
        $this->acronym = $acronym;

        return $this;
    }

    public function getDegreeLevel(): ?string
    {
        return $this->degree_level;
    }

    public function setDegreeLevel(string $degree_level): self
    {
        $this->degree_level = $degree_level;

        return $this;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        // set (or unset) the owning side of the relation if necessary
        $newDegree = null === $candidate ? null : $this;
        if ($candidate->getDegree() !== $newDegree) {
            $candidate->setDegree($newDegree);
        }

        return $this;
    }



}
