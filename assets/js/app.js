/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

/*
var $collectionHolder;

const $ = ('jquery');
// setup an "add a Degree" link
var $addDegreeButton = $('.add_degree_link');
var $newLinkLi = $('#degreeName').append($addDegreeButton);

$(document).ready(function() {
    // Get the ul that holds the collection of Degrees
    $collectionHolder = $('#degreeName');

    // add the "add a Degree" anchor and li to the Degrees ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addDegreeButton.on('click', () => {
            // add a new Degree form (see next code block)
            addDegreeForm($collectionHolder, $newLinkLi);
        });

    function addDegreeForm($collectionHolder, $newLinkLi) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');
    
        // get the new index
        var index = $collectionHolder.data('index');
    
        var newForm = prototype;
        // You need this only if you didn't set 'label' => false in your Degrees field in TaskType
        // Replace '__name__label__' in the prototype's HTML to
        // instead be a number based on how many items we have
        // newForm = newForm.replace(/__name__label__/g, index);
    
        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        newForm = newForm.replace(/__name__/g, index);
    
        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);
    
        // Display the form in the page in an li, before the "Add a Degree" link li
        var $newFormLi = $('#degreeName').append(newForm);
        $newLinkLi.before($newFormLi);
    }
    
});


*/


